import os
import numpy as np
from scipy.io import loadmat
from IPython import embed

class Evaluator():
    def __init__(self):
        self.q_keyframes = np.loadtxt( "indices/instre/qimlist.txt",\
                                      dtype='str', delimiter='\n' )

        self.keyframes = np.loadtxt( "indices/instre/imlist.txt",\
                                      dtype='str', delimiter='\n' )

        self.keyframes = np.array([n.split('.')[0] for n in self.keyframes])
        self.q_keyframes = np.array([n.split('.')[0] for n in self.q_keyframes])

        self.map_ima_name={}
        for i, name in enumerate(self.keyframes):
            self.map_ima_name[name]=i

        self.gnd = get_gnd()

    def compute_map( self, ranks, verbose=False, per_query=False):
        gnd = self.gnd
        mAP = 0.0
        nq = len(gnd)
        aps = np.zeros(nq)
        precision = []
        # for each query
        for i in range(nq):
            gt_list = gnd[i].ok
            intersection = np.in1d(ranks[:,i], gt_list)
            pos = np.arange(ranks[:,i].shape[0])[intersection]
            ap = score_ap_from_ranks( pos, gt_list.shape[0] )
            if verbose:
                print "query{} -- {}".format( i, ap )
            mAP += ap
            precision.append( ap )
        if per_query:
            return np.array(precision)
        else:
            return mAP/nq


    def compute_ap_for_image(self, id_img, list_names):
        if id_img not in self.q_keyframes:
            return None
        else:
            id_query = np.where( id_img == self.q_keyframes )[0][0]
            rank = np.array([ self.map_ima_name[name] for name in list_names ])
            # map list names to indices
            gnd = self.gnd
            mAP = 0.0
            nq = len(gnd)
            aps = np.zeros(nq)
            # for A query

            gt_list = gnd[id_query].ok
            intersection = np.in1d(rank, gt_list)
            pos = np.arange(rank.shape[0])[intersection]
            ap = score_ap_from_ranks( pos, gt_list.shape[0] )
            return ap


# ============ FUNCTIONS =======================================================


def get_gnd():
    data_gnd = loadmat( 'evaluate/instre/gnd_instre.mat' )
    class Query():
        def __init__(self, ok, bbx):
            self.ok = ok[0,:].squeeze()
            self.bbx = bbx[0,:].squeeze()

    def convert_gnt( data_gnd ):
        # for each quey I have the ok list and
        gt_queries = []
        for i in range( data_gnd['gnd'].shape[1] ):
            gt_queries.append( Query( data_gnd['gnd'][0][i][0], data_gnd['gnd'][0][i][1] ) )
        return gt_queries
    return convert_gnt( data_gnd )

def score_ap_from_ranks( _ranks, nres ):
    _ranks = _ranks.astype(float)
    nimgranks = _ranks.shape[0]
    ap = 0
    recall_step = 1.0/nres
    for j in range(1, nimgranks+1):
        rankval = _ranks[j-1]

        if rankval == 0:
            p0 = 1.0
        else:
            p0 = (j-1.0)/rankval

        p1 = j/(rankval+1)
        ap = ap+(p0+p1)*recall_step/2
    return ap
