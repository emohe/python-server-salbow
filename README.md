# server-python-salbow

This repository uses [salbow](https://github.com/imatge-upc/salbow) search engine.
The engine is implemented in a python class (salbow_retrieval/BLCF_engine) with four main functionalities:

- `get_rank_for_dataset_image`:
    Given an image_id from the dataset, it returns a list of relevant images to display
- `get_rank_for_image_array`:
    Given numpy array containing an image (python-opencv format), it returns a list of relevant images to display
- `query_expansion`:
    Given a list of relevant image_ids, it performs average query expansion and returns a list of relevant images to display
- `annotations`:
  Given a list of relevant image_ids and unrelevant image_ids, it trains a linear SVM and returns a list of relevant images to display


> The engine class is initialized loading a list of keyframes and queries from a particular dataset (indies/[dataset]/[imlist][qimlist].txt), and its pre-computed descriptors.
>
> Precomputed descriptors are stored in a numpy sparse matrix format (data/[dataset]/[queries][targets_salgan].npz), which contain bow descriptos of 25.000k dimensions
where each visial work has been weighted by a saliency model [salgan](https://github.com/imatge-upc/saliency-salgan-2017).

* For more info check [salbow arxiv paper](https://arxiv.org/pdf/1711.10795.pdf).

### how to install
(Tested on Ubuntu 16.04 system and MacOS Sierra)

* Create a virtualenv using python2.7 as explained in the [reactjs repository](https://github.com/paulagd/react-visualization-tool-CBIR).

```
 pip install -r requirements.txt
```

* Make sure you are using tensorflow order for the images. Edit '~/.keras/keras.json'
```
{    "image_dim_ordering": "tf", "epsilon": 1e-07, "floatx": "float32", "backend": "tensorflow"}
```

### pre-computed descriptors
We provide pre-computed descriptors for Oxford, Paris and INSTRE datasets. They are automatically downloaded in 'data' folder, as well as PCA and the centroids for the BLCF model.

### computing descriptors for a new images

The class 'Extractor' encodes the image following the BLCF approach. It uses pre-trained VGG16 and the precomputed PCA and centroids.
If you want to make use of GPU to speed-up the calculation, make sure you install [tensorflow with GPU support](https://www.tensorflow.org/install/install_linux).

### evaluating performance

The folder 'evaluate' contains scripts to evaluate the performance of the ranks in Oxford, Paris and INSTRE.

### Repositories related:
>
> * [React visualization tool for CBIR](https://github.com/paulagd/react-visualization-tool-CBIR)
> * [Nodejs server](https://github.com/paulagd/node-server)
> * [python-server-dummy](https://bitbucket.org/emohe/python-server-dummy/src/master/)


### Execution

* To run the code run:
```
python main.py    
```

### Documentation

Install apidoc
```
npm install -g apidoc

```

Run documentation generation script:
```
apidoc -o documentation -e node_modules

```

> Open the file 'index.html' stored in the folder 'documentation' to see how to customize the system.
