import os
import numpy as np
from sklearn.preprocessing import normalize
from sklearn.svm import LinearSVC
from utils import load_sparse_csr
from IPython import embed
from extractor import Extractor
from utils import(
    compute_ranks,
    compute_similarity)

'''
Bags of Local Convolutional features (BLCF) search engine.

It uses pre-computed descriptors weighted with saliency
[salbow](https://github.com/imatge-upc/salbow)
[BLCF](https://github.com/imatge-upc/retrieval-2016-icmr)
'''

class BLCF_engine():
    def __init__(self, dataset):
        self.dataset = dataset

        # load indices for dataset
        self.keyframes = np.loadtxt( "indices/{}/imlist.txt".format(dataset),\
                                      dtype='str', delimiter='\n' )
        self.q_keyframes = np.loadtxt( "indices/{}/qimlist.txt".format(dataset),\
                                      dtype='str', delimiter='\n' )

        # remove extension
        self.keyframes = np.array([n.split('.')[0] for n in self.keyframes])
        self.q_keyframes = np.array([n.split('.')[0] for n in self.q_keyframes])

        # compute/load descriptors
        # L2-normalize descriptors to compute cosine similarity
        self.targets = normalize(load_sparse_csr(os.path.join('salbow_retrieval/data', dataset, 'targets_salgan.npz')))
        self.queries = normalize(load_sparse_csr(os.path.join('salbow_retrieval/data', dataset, 'queries.npz')))

        # compute dataset ranks for original descriptors
        similarity = compute_similarity(self.targets, self.queries)
        self.ranks = compute_ranks(similarity) # N_targets, N_queries
        self.new_ranks = self.ranks # N_targets, N_queries

        # feature Extractor
        self.extractor = Extractor(dataset)


    def _get_computed_descriptor(self, id_img):
        # check belongs to queries ...
        q_descriptor=None
        mode_query = True
        if id_img in self.q_keyframes:
            idx = np.where( id_img==self.q_keyframes )[0][0]
            q_descriptor = self.queries[idx,...]

        # it belongs to targets
        elif id_img in self.keyframes:
            idx = np.where( id_img==self.keyframes )[0][0]
            q_descriptor = self.targets[idx,...]
            mode_query = False
        else:
            print("query image not from the dataset!")
            q_descriptor = None
            idx = None
            mode_query = None
            print(id_img)

        print(idx, mode_query)
        return q_descriptor, idx, mode_query

    def get_rank_for_dataset_image(self, id_img, return_indices=False):
        # check belongs to queries ...
        if id_img in self.q_keyframes:
            idx = np.where( id_img==self.q_keyframes )[0][0]
            rank_indices = self.ranks[:,idx]

        # it belongs to targets
        elif id_img in self.keyframes:
            idx = np.where( id_img==self.keyframes )[0][0]
            q_descriptor = self.targets[idx,...]
            similarity = compute_similarity(self.targets, q_descriptor)
            rank_indices = compute_ranks(similarity).squeeze()
        else:
            print("query image not from the dataset!")
            print(id_img)

        if return_indices:
            return rank_indices
        else:
            list_names = self.keyframes[rank_indices]
            return list_names

    def get_rank_for_image_array(self, ima, return_indices=False):
        # get features for new image
        #q_descriptor = process_image(ima)
        q_descriptor = self.extractor.get_representation(ima)
        # compute similarity and rank for image
        similarity = compute_similarity(self.targets, q_descriptor)
        rank_indices = compute_ranks(similarity).squeeze()

        if return_indices:
            return rank_indices
        else:
            list_names = self.keyframes[rank_indices]
            return list_names

    def query_expansion(self, id_img, similar_list, init_new_ranks=False, ima=None):
        """
        Query expansion -- only for images with already computed descriptors...
        """

        # get descriptors for query and selected images...
        q_descriptor = []
        if ima is None and id_img=="unknown_id":
                descriptor=None
                mode_query=False
        elif id_img!="unknown_id":
                descriptor, idx, mode_query = self._get_computed_descriptor(id_img)
                descriptor = np.array(descriptor.toarray())
                q_descriptor.append(descriptor)
        else:
            mode_query=False
            # compute descriptor
            descriptor = self.extractor.get_representation(ima)
            descriptor = np.array(descriptor.toarray())
            q_descriptor.append(descriptor)

        # add query
        for name in similar_list:
            descriptor, _, _= self._get_computed_descriptor(name)
            descriptor = np.array(descriptor.toarray())
            q_descriptor.append(descriptor)
        q_descriptor = np.array(q_descriptor).squeeze()

        # compute the average vector
        q_descriptor = q_descriptor.mean(axis=0, keepdims=True)

        # compute similarity and rank for image
        similarity = compute_similarity(self.targets, q_descriptor)
        rank_indices = compute_ranks(similarity).squeeze()

        # if mode query update ranks
        if mode_query:
            if init_new_ranks:
                self.new_ranks = self.ranks
            self.new_ranks[:,idx] = rank_indices

        # final list of images
        list_names = self.keyframes[rank_indices]
        return list_names

    def annotations(self, id_img, similar_list, init_new_ranks=False, ima=None):
        """
        Simple relevance feedback based on possitive and negative annotations
        """


        # get rank for image
        if ima is None:
            indices_rank = self.get_rank_for_dataset_image(id_img, return_indices=True)
        else:
            indices_rank = self.get_rank_for_image_array( ima,return_indices=True)


        targets  = self.targets[indices_rank,...]
        keyframes = self.keyframes[indices_rank]

        # get annotations ======================================================
        p_annotations = similar_list['positive']
        n_annotations = similar_list['negative']

        # Get possitive examples...
        X_possitive = []

        # check whether the image belongs to the query set
        if ima is None:
            descriptor, idx, mode_query = self._get_computed_descriptor(id_img)
        else:
            mode_query=False
            # compute descriptor
            descriptor = self.extractor.get_representation(ima)

        descriptor = np.array(descriptor.toarray())
        X_possitive.append(descriptor)

        for name in p_annotations:
            descriptor, _, _ = self._get_computed_descriptor(name)
            descriptor = np.array(descriptor.toarray())
            X_possitive.append(descriptor)
        X_possitive = np.array(X_possitive).squeeze()

        if len(X_possitive.shape)<2:
            X_possitive = np.expand_dims(X_possitive, axis=0)

        count_p = X_possitive.shape[0]

        # Get negative examples...
        X_negative = []
        for name in n_annotations:
            descriptor, _, _ = self._get_computed_descriptor(name)
            descriptor = np.array(descriptor.toarray())

            X_negative.append(descriptor)

        X_negative = np.array(X_negative).squeeze()
        if len(X_negative.shape)<2:
            X_negative = np.expand_dims(X_negative, axis=0)
        count_n = X_negative.shape[0]



        # create the labels matrix with the first count_p rows to 1
        Y = np.zeros( (count_p+count_n) )
        Y[:count_p]=1

        X = np.vstack([X_possitive,X_negative])

        # Apply SVM model ======================================================
        model = LinearSVC()

        # train
        model.fit(X, Y)

        # predict on features and re-sort based on scores from SVM
        scores_samples = model.decision_function(targets)
        new_rank_indices = np.argsort( scores_samples )[::-1]

        # final list order
        list_names = keyframes[new_rank_indices]

        if mode_query:
            if init_new_ranks:
                self.new_ranks = self.ranks
            # reorder original rank indices to update ranks
            indices_rank = indices_rank[new_rank_indices]
            self.new_ranks[:,idx] =  indices_rank

        return list_names
