from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from sklearn.preprocessing import normalize
from IPython import embed

class TFCodebook():
    def __init__(self, centroids, ndim=512):
        self.x = tf.placeholder(tf.float32, [None, ndim])
        with tf.Session() as sess:
            n_centroids = sess.run(tf.nn.l2_normalize(centroids, axis=1))

        self.centroids = tf.constant(n_centroids, dtype=tf.float32)
        # graph operation
        self.distances = tf.matmul(self.x, tf.transpose(self.centroids))

    def compute_distances(self, data):
        with tf.Session() as sess:
            n_data = sess.run(tf.nn.l2_normalize(data, axis=1))
            # compute distances
            comp_distances = sess.run(self.distances, feed_dict={self.x: n_data})
        return comp_distances

    def get_assignments(self, data):
        distances = self.compute_distances(data)
        return np.argmax(distances, axis=1)

    def _close():
        self.sess.close()


if __name__ == '__main__':
    centroids = np.random.random((1000,512))
    centr_norm = normalize(centroids)
    data = np.random.random((25000,512))

    codebook = TFCodebook( centroids )

    distances = codebook.get_assignments(data)
    print(distances.shape)
    embed()
