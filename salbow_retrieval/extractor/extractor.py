import os
import numpy as np
from vgg import init_model
from vgg_utils import preprocess_image
from vgg_utils import preprocess_image
from tf_codebook import TFCodebook
from IPython import embed
from scipy.ndimage import zoom
from sklearn.preprocessing import normalize
from collections import Counter
from scipy.sparse import coo_matrix
from scipy.sparse import hstack, vstack

def get_bow( assignments, weights=None, n=25000 ):
    # sparse encoding !
    rows = np.array([], dtype=np.int)
    cols = np.array([], dtype=np.int)
    vals = np.array([], dtype=np.float)
    n_docs = 0

    # get counts
    cnt = Counter(assignments.flatten())
    ids = np.array(cnt.keys())
    if weights == None:
        weights = np.array(cnt.values())
    else:
        weights = weights.flatten()
        weights = np.array([weights[np.where(assignments.flatten()==i)[0]].sum() for i in ids])

    #save index
    cols = np.append( cols, np.array(ids).astype(int) )
    rows = np.append( rows, np.ones( len(cnt.keys()), dtype=int )*n_docs )
    vals = np.append( vals, weights.astype(float) )
    n_docs +=1

    bow = coo_matrix( ( vals, (rows, cols) ), shape=(n_docs,n) )
    bow = bow.tocsr()
    bow = normalize(bow)
    return bow


def postprocess(feat, get_dim=False, interpolate=1, apply_pca=None):
    if interpolate > 1:
        z= interpolate
        feat = zoom(feat, (1,z,z), order=1)

    dim = feat.shape
    feat = np.reshape( feat, (feat.shape[0], -1) )
    feat = np.transpose( feat, (1,0) )
    feat = normalize(feat)
    if apply_pca is not None:
        feat = apply_pca.transform(feat)
        feat = normalize(feat)
    if get_dim:
        return feat.astype(np.float32), dim
    else:
        return feat.astype(np.float32)


class Extractor():
    def __init__(self, dataset):
        self.vgg = init_model()
        self.pca_model = np.load(os.path.join( "salbow_retrieval/data", dataset, "pca.npy" )).tolist()
        self.centroids = np.load(os.path.join( "salbow_retrieval/data", dataset, "centroids.npy" ))
        self.codebook = TFCodebook( self.centroids )

    def get_representation( self, ima_path ):
        """
        Get BoW representation for the image path
        """
        feats = self.vgg.predict( preprocess_image(ima_path) ).squeeze()
        # switch dimensions
        feats = np.transpose(feats, (2,0,1))

        feats = postprocess(feats, interpolate=2, apply_pca=self.pca_model)
        assignments = self.codebook.get_assignments(feats)
        bow_image = get_bow( assignments )
        return bow_image
