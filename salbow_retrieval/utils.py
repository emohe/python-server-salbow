import numpy as np
from IPython import embed
from scipy.sparse import csr_matrix



def compute_similarity(targets, queries):
    """
    This function computes the distance between targets and queries.
    we use the cosine similarity between l2-norm vectors
    (Addapted to sparce matrices)

    returns (N_target,N_queries) similarity ranks
    """
    similarities = targets.dot( queries.T )
    if 'sparse' in str(type(similarities)):
        similarities = np.array(similarities.toarray())
    similarities = similarities.squeeze()
    if len(similarities.shape)<2:
        similarities = np.expand_dims(similarities, axis=0).T

    return similarities


def compute_ranks( similarity_matrix ):
    """
    Compute ranks based on a similarity matrix
    """

    N = similarity_matrix.shape[1]

    ranks = []
    for i in range(N):
        rank = np.argsort( similarity_matrix[:,i] )[::-1]
        ranks.append(rank)
    return np.array(ranks).T


# ====== CUSTOM BLCF
def load_sparse_csr(filename):
    # load sparse matrix with pre-computed descriptors
    loader = np.load(filename )
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                      shape=loader['shape'])
